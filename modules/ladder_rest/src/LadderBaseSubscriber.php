<?php

namespace Drupal\ladder_rest;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;

/**
 * LadderBaseSubscriber service class.
 */
class LadderBaseSubscriber {

  /**
   * The database class.
   *
   * @var \Drupal\Core\Database\Database
   */
  private $database;

  /**
   * Construct function of the class.
   */
  public function __construct(connection $database) {
    $this->database = $database;
  }

  /**
   * Get taxonomy term id.
   */
  public function getTermData($tag = []) {

    if (isset($tag['is_new'])) {

      $tid = db_select('taxonomy_term_field_data', 't')
        ->fields('t', ['tid'])
        ->condition('vid', 'ladder_tags', '=')
        ->condition('name', $tag['name'], '=')
        ->execute()
        ->fetchField();
      if ($tid) {
        $step['tags'] = $tid;
      }
      else {
        $step['tags'] = $this->createNewTags($tag);
      }
    }
    else {
      $step['tags'] = $tag['id'];
    }
    return $step['tags'];
  }

  /**
   * Create new taxonomy term.
   */
  public function createNewTags($tag = []) {

    // Create new tag.
    $term = Term::create([
      'vid' => 'ladder_tags',
      'name' => $tag['name'],
    ]);
    $term->save();
    return $term->id();
  }

  /**
   * Create new remote video .
   */
  public function checkRemoteVideoExist($video_id = '') {

    $mid = '';

    // Check if remote video exist or not.
    if (!empty($video_id)) {
      $database = $this->database;
      $query = $database->select('media__field_media_oembed_video', 'm');
      $query->fields('m', ['entity_id']);
      $query->condition('m.bundle', 'remote_video');
      $query->condition('m.deleted', 0);
      $query->condition('m.field_media_oembed_video_value', $video_id);
      $result = $query->execute();
      $mid = $result->fetchField();
    }
    return $mid;
  }

  /**
   * Create new remote video .
   */
  public function createRemoteVideo($video = []) {

    $video['name'] = '';
    $video['url'] = '';
    // Check if remote media exist or not.
    $mid = $this->checkRemoteVideoExist($video['url']);
    // Create new midea if not exist.
    if (empty($mid)) {
      $media = Media::create([
        'bundle'      => 'remote_video',
        'uid'         => \Drupal::currentUser()->id(),
        'name'        => $video['name'],
        'field_media_oembed_video' => [
          'value' => $video['url'],
        ],
      ]);
      $media->save();
      $mid = $media->id();
    }
    return $mid;
  }

}
