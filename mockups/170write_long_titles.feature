# this test is to ensure no one arbitrarily shortens character counts - please
# permit typical lengths even though our current UI may seem to suggest
# shortening them; as we learn more, we'll adjust UI to meet user needs.

# related issue: https://www.drupal.org/project/drupalladder/issues/3150699

Feature: very important: maintain default character counts on fields and textareas
  As anon or logged-in user
  I can write long text whether in title field, other field or textarea
  So that I can save information without being forced to edit before saving
