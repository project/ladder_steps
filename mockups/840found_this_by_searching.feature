Feature: found this by searching for
  As a visitor on a ladder page
  As a visitor on https://www.yelp.com/biz/innas-kitchen-culinaria-newton?osq=Gluten+Free+Restaurants
  I can see
  ```
  People found Inna's Kitchen Culinaria by searching for…
  Catering Restaurants Newton
  Gluten Free Bakery Newton
  Gluten Free Jewish Newton
  [...]
  ```
  So that I consider similar searches that might suit my needs or expand my thinking
  So that each 'arrival' is also a jumping off point in my continuing education
