Feature: filter steps to reflect my situation
  As any user (anonymous or logged in)
  I can filter steps by various criteria
  So that the list of instructions matches my personal situation

  Scenario: filter by operating system
    Given I am on /git
    When I select the tag "Ubuntu >= 18"
    Then I can see "apt-get install git"
    And I cannot see "Download the latest Git for Windows installer"
    When I deselect the tag "Ubuntu >= 18"
    And I select the tag "Windows 10"
    Then I can "Download the latest Git for Windows installer"
    And I cannot see "apt-get install git"

  @regression
  Scenario: filter change has no effect on expand/collapse state
    Given I am on /git
    And I expand "Download the latest Git for Windows installer"
    Then I can see the child step "Follow the installation wizard to complete the installation"
    And when I select the tag "Windows 10"
    Then I can still see the child step "Follow the installation wizard to complete the installation"

  @not_mvp
  Scenario: see indication of steps hidden by filters
    Given I am on /git and have selected the tag "Ubuntu >= 18"
    Then I should see ...
