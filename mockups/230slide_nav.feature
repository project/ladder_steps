Feature: navigate slides while presenting
  As any user
  I can present steps in a Ladder as a series of slides
  So that large groups of people (e.g. DrupalCon session attendees) can follow my presentation

  Scenario: slide progression with buttons
  # for an example of 4-button nav functions see https://infinite-red.slides.com/gantlaborde/adventures-in-ai-javascript
  Given I am visiting a Ladder in Lecture mode
  When I click Next
  Then I see the subsequent sibling slide
  When I click Down
  Then I see the child slide
  When I click Up
  Then I see the parent slide
  When I click Previous
  Then I see the antecedent sibling slide
  When one of these options has no available slide
  Then I see the corresponding nav button disabled
