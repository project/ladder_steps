Feature: very important: quick change and save
  As anon or logged-in user
  I can fix a minor error quickly and save
  So that I feel no loss of productivity when I decide to make a quick change
